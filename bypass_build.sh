#!/bin/sh -e
# Fetches artifacts from a previous job to avoid rebuilding the kernel while testing

if [ -z "$USE_PREV_ARTIFACTS" ]; then
	echo "USE_PREV_ARTIFACTS variable not set!"
	exit 1
fi

mkdir -p packages/edge/aarch64/ || true

wget -P packages/edge/aarch64/ "$USE_PREV_ARTIFACTS"
wget -P packages/edge/aarch64/ $(dirname "$USE_PREV_ARTIFACTS")/APKINDEX.tar.gz

exit 0
