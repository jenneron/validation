#!/bin/sh -e
# Build and test the ramdisk for a given device

# Can be set in the environment
# BOOTIMG
# SUCCESS_STRING
# Must be set in the environment
# CDBA_DEVICE
# DEVICE (if BOOTIMG is not set)

curl --location --output /tmp/cdba.apk --location \
	https://connolly.tech/cdba.apk
sudo apk add --allow-untrusted /tmp/cdba.apk

echo "Current user is $GITLAB_USER_NAME ($GITLAB_USER_LOGIN)"

# CDBA_DEVICE comes from environment
CDBA_HOST="vault.connolly.tech"
CDBA_PORT="2233"

mkdir -p ~/.ssh
cat > ~/.ssh/config <<EOF
Host tiger.cdba
	User cdba
	Port $CDBA_PORT
	HostName $CDBA_HOST
	StrictHostKeyChecking no
EOF

chown -R "$(id -u):$(id -g)" ~/.ssh
chmod 700 ~/.ssh

eval $(ssh-agent -s)
echo "$CDBA_SSH_KEY" | tr -d '\r' | ssh-add -

BOOTIMG=${BOOTIMG:-/tmp/"$DEVICE"-boot.img}

if [ ! -f "$BOOTIMG" ]; then
	echo "$BOOTIMG does not exist"
	exit 1
fi

echo "Booting $BOOTIMG on $CDBA_DEVICE"

# Timeout after 30 seconds of inactivity
# Power off board and quit if the board prints 20 '~' characters (-c 0)
cdba -T 30 -c 0 -h tiger.cdba -b $CDBA_DEVICE $BOOTIMG | tee /tmp/cdba.log

sudo cp /tmp/cdba.log cdba.log

SUCCESS_STRING=${SUCCESS_STRING:-PMOS-CI-OK}

if grep -q "$SUCCESS_STRING" /tmp/cdba.log; then
	echo "CDBA: All tests pass"
else
	exit 1
fi
