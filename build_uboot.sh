# Build U-Boot and generate images for SDM845 devices

set -e

DEVICES="
	enchilada:sdm845-oneplus-enchilada
	fajita:sdm845-oneplus-fajita
	axolotl:sdm845-shift-axolotl
	beryllium-tianma:sdm845-xiaomi-beryllium-tianma
	beryllium-ebbg:sdm845-xiaomi-beryllium-ebbg
"

make CROSS_COMPILE=aarch64-alpine-linux-musl- qcom_defconfig

make CROSS_COMPILE=aarch64-alpine-linux-musl- -j$(nproc)

mkdir binaries

gzip u-boot-nodtb.bin

for device in $DEVICES; do
	DTB=$(echo $device | cut -d: -f2)
	DEVICE=$(echo $device | cut -d: -f1)

	cat u-boot-nodtb.bin.gz arch/arm/dts/$DTB.dtb > u-boot-$DEVICE.bin.gz
	mkbootimg-osm0sis --base 0x0 \
		--kernel_offset 0x8000 \
		--ramdisk_offset 0x01000000 \
		--tags_offset 0x100 \
		--pagesize 4096 \
		--kernel u-boot-$DEVICE.bin.gz \
		-o binaries/u-boot-$DEVICE.img
done

